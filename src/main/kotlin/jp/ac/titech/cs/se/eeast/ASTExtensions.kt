package jp.ac.titech.cs.se.eeast

import jp.ac.titech.cs.se.eeast.helper.ASTHelper
import org.eclipse.jdt.core.dom.*
import java.io.File

/*
 * Adds function extensions to manage ASTNode
 */
//interface ASTExtensions {
    fun CompilationUnit.getRange(node: ASTNode) = ASTHelper.getRange(this, node).run { this[0]..this[1] }

    fun ASTNode.resolveAppropriateBinding() : IBinding? = ASTHelper.resolveBinding(this)

    fun BodyDeclaration.obtainIdentifier() = ASTHelper.obtainIdentifier(this)

    fun Type.obtainIdentifier() = ASTHelper.obtainIdentifier(this)

    fun Name.obtainIdentifier() = ASTHelper.obtainIdentifier(this)

    fun ASTNode?.isRenamable() = ASTHelper.isRenamableDeclaration(this)
//}

/*
 * Adds function extensions to visit/accept ASTNode
 */
//interface ASTWalkExtensions {
    fun File.createCompilationUnit() =
            ASTProvider.getCompilationUnit(this).run {
                if (isPresent) get() else null
            }

    fun ASTVisitorDispatcher.type() = addVisitor(TypeDeclarationVisitor())

    fun ASTVisitorDispatcher.method() = addVisitor(MethodDeclarationVisitor())

    fun ASTVisitorDispatcher.field() = addVisitor(FieldDeclarationVisitor())

    fun ASTVisitorDispatcher.getTypeVisitor(p: (ITypeSpecificASTVisitor<TypeDeclaration>) -> Boolean = { true }) =
            getVisitorList(TypeDeclaration::class.java).firstOrNull {
                // Type safe cast
                p(it as ITypeSpecificASTVisitor<TypeDeclaration>)
            }

    fun ASTVisitorDispatcher.getMethodVisitor(p: (ITypeSpecificASTVisitor<MethodDeclaration>) -> Boolean = { true }) =
            getVisitorList(MethodDeclaration::class.java).firstOrNull {
                // Type safe cast
                p(it as ITypeSpecificASTVisitor<MethodDeclaration>)
            }

    fun ASTVisitorDispatcher.getFiledVisitor(p: (ITypeSpecificASTVisitor<FieldDeclaration>) -> Boolean = { true }) =
            getVisitorList(FieldDeclaration::class.java).firstOrNull {
                // Type safe cast
                p(it as ITypeSpecificASTVisitor<FieldDeclaration>)
            }
//}