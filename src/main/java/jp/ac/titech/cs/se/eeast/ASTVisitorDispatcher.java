package jp.ac.titech.cs.se.eeast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ASTVisitorDispatcher extends ASTVisitor {
    private static final String TAG = ASTVisitorDispatcher.class.getSimpleName();

    private static final Logger log = LoggerFactory.getLogger(TAG);

    private Map<Class<? extends ASTNode>, List<ITypeSpecificASTVisitor<? extends ASTNode>>> mTypeSpecificASTVisitorListMap;

    public ASTVisitorDispatcher() {
        mTypeSpecificASTVisitorListMap = new HashMap<>();
    }

    public ASTVisitorDispatcher addVisitor(ITypeSpecificASTVisitor<?> visitor) {
        if (visitor == null) {
            return this;
        }

        List<ITypeSpecificASTVisitor<?>> visitorList = mTypeSpecificASTVisitorListMap
                .get(visitor.getAcceptableClass());

        if (visitorList == null) {
            visitorList = new ArrayList<>();
            mTypeSpecificASTVisitorListMap.put(visitor.getAcceptableClass(),
                    visitorList);
        }

        if (!visitorList.contains(visitor)) {
            visitorList.add(visitor);
        }

        return this;
    }

    public ASTVisitorDispatcher removeVisitor(ITypeSpecificASTVisitor<?> visitor) {
        List<ITypeSpecificASTVisitor<?>> visitorList = mTypeSpecificASTVisitorListMap
                .get(visitor.getAcceptableClass());

        if (visitorList != null) {
            visitorList.remove(visitor);
        }

        return this;
    }

    public void removeAllVisitor() {
        mTypeSpecificASTVisitorListMap.clear();
    }

    public Map<Class<? extends ASTNode>, List<ITypeSpecificASTVisitor<?>>> getVisitorListMap() {
        return mTypeSpecificASTVisitorListMap;
    }

    public <T extends ASTNode> List<ITypeSpecificASTVisitor<? extends ASTNode>> getVisitorList(
            Class<T> cls) {
        return mTypeSpecificASTVisitorListMap.get(cls);
    }

    @Override
    public boolean preVisit2(ASTNode node) {
        try {
            if (mTypeSpecificASTVisitorListMap.containsKey(node.getClass())) {
                List<ITypeSpecificASTVisitor<?>> visitorList = mTypeSpecificASTVisitorListMap
                        .get(node.getClass());

                for (ITypeSpecificASTVisitor<?> visitor : visitorList) {
                    if (visitor.isEnable() && visitor.isAcceptable(node)) {
                        visitor.setEnable(visitor.onVisitNode(node));
                    }
                }
            }
        } catch (ITypeSpecificASTVisitor.UnhandleASTNodeException e) {
            log.debug("Exception catched. {}", e.toString());
        }

        return super.preVisit2(node);
    }
}
