package jp.ac.titech.cs.se.eeast;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;

public abstract class AbstractASTNodeVisitor<T extends ASTNode> implements
        ITypeSpecificASTVisitor<T> {
    private boolean mIsEnabled = true;
    private boolean mIsVisited = false;
    private List<T> mResultList = new ArrayList<T>();

    @Override
    public boolean isEnable() {
        return mIsEnabled;
    }

    @Override
    public void setEnable(boolean isEnable) {
        mIsEnabled = isEnable;
    }

    @Override
    public boolean isAcceptable(ASTNode node) {
        return getAcceptableClass().isInstance(node);
    }

    @Override
    public boolean isVisited() {
        return mIsVisited;
    }

    @Override
    public void setVisited(boolean isVisited) {
        mIsVisited = isVisited;
    }

    @Override
    public List<T> getResult() {
        return mResultList;
    }

    @Override
    public void addResult(T result) {
        mResultList.add(result);
    }

    @Override
    public void clearResult() {
        mResultList.clear();
    }

    @Override
    public void clear() {
        mResultList.clear();
        mIsEnabled = true;
        mIsVisited = false;
    }
}
