package jp.ac.titech.cs.se.eeast;

import jp.ac.titech.cs.se.eeast.helper.Utilities;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.*;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

public class ASTProvider {
    public enum JDK {
        V_1_6, V_1_7, V_1_8;

        public String toJavaCoreFormat() {
            switch (mJDK) {
                case V_1_6:
                    return JavaCore.VERSION_1_6;
                case V_1_7:
                    return JavaCore.VERSION_1_7;
                case V_1_8:
                    return JavaCore.VERSION_1_8;
            }

            return JavaCore.VERSION_1_6;
        }
    }

    private static JDK mJDK = JDK.V_1_8;

    public static void setJDKVersion(JDK jdk) {
        Objects.nonNull(jdk);
        mJDK = jdk;
    }

    protected static ASTParser configuredParser() {
        ASTParser parser = ASTParser.newParser(AST.JLS4);

        Hashtable options = JavaCore.getOptions();
        JavaCore.setComplianceOptions(mJDK.toJavaCoreFormat(), options);

        parser.setCompilerOptions(options);
        parser.setKind(ASTParser.K_COMPILATION_UNIT);

        return parser;
    }

    public static Optional<CompilationUnit> getCompilationUnit(String path) {
        return Utilities.readContent(path).map(content -> {
            ASTParser parser = configuredParser();
            parser.setSource(content.toCharArray());
            return (CompilationUnit) parser.createAST(new NullProgressMonitor());
        });
    }

    public static Optional<CompilationUnit> getCompilationUnit(File file) {
        return Utilities.readContent(file).map(content -> {
            ASTParser parser = configuredParser();
            parser.setSource(content.toCharArray());
            return (CompilationUnit) parser.createAST(new NullProgressMonitor());
        });
    }

    public static CompilationUnit getCompilationASTNode(ICompilationUnit unit) {
        ASTParser parser = configuredParser();
        parser.setSource(unit);
        return (CompilationUnit) parser.createAST(new NullProgressMonitor());
    }

    public static CompilationUnit getCompilationASTNode(String source) {
        ASTParser parser = configuredParser();
        parser.setSource(source.toCharArray());
        return (CompilationUnit) parser.createAST(new NullProgressMonitor());
    }

    public static CompilationUnit getTypeASTNode(IType type) throws JavaModelException {
        return getMemberASTNode(type);
    }

    public static CompilationUnit getMethodASTNode(IMethod method)
            throws JavaModelException {
        return getMemberASTNode(method);
    }

    public static CompilationUnit getFieldASTNode(IField field)
            throws JavaModelException {
        return getMemberASTNode(field);
    }

    private static CompilationUnit getMemberASTNode(IMember member)
            throws JavaModelException {
        ASTParser parser = configuredParser();
        parser.setSource(member.getCompilationUnit());
        ISourceRange range = member.getSourceRange();
        parser.setSourceRange(range.getOffset(), range.getLength());
        return (CompilationUnit) parser.createAST(new NullProgressMonitor());
    }

    /**
     * Creates compilation unit from relative file path via chunk.
     *
     * @param relPath relative path from the root of workspace
     * @return
     */
    public static Optional<CompilationUnit> getCompilationUnitWithRelativePath(String relPath) {
        return Utilities.readContent(((IFile) ResourcesPlugin.getWorkspace().getRoot()
                .findMember(relPath))).map(content -> {
            ASTParser parser = configuredParser();

            int idx = relPath.indexOf("/", 2);

            if (idx > 1) {
                getProject(relPath.substring(1, idx)).map(JavaCore::create).ifPresent(parser::setProject);
            }

            parser.setIgnoreMethodBodies(false);
            parser.setResolveBindings(true);
            parser.setBindingsRecovery(true);
            parser.setStatementsRecovery(true);
            parser.setKind(ASTParser.K_COMPILATION_UNIT);
            parser.setSource(content.toCharArray());
            return (CompilationUnit) parser.createAST(new NullProgressMonitor());
        });
    }

    private static Optional<IProject> getProject(String name) {
        if (StringUtils.isEmpty(name)) {
            return Optional.empty();
        }

        return Stream.of(ResourcesPlugin.getWorkspace().getRoot().getProjects())
                .filter(s -> s.getName().equals(name)).findFirst();
    }
}
