package jp.ac.titech.cs.se.eeast.helper;

import java.util.List;
import java.util.stream.Stream;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.*;
import org.eclipse.jdt.core.util.CompilationUnitSorter;

/**
 * This adds the types to AST classes.
 */
@SuppressWarnings("unchecked")
public class ASTHelper {
    private static final String TAG = ASTHelper.class.getSimpleName();

    private ASTHelper() {
    }

    public static int[] getRange(CompilationUnit receiver, ASTNode target) {
        int[] range = new int[2];
        range[0] = receiver.getLineNumber(target.getStartPosition()) - 1;
        range[1] = receiver.getLineNumber(target.getStartPosition() + target.getLength()) - 1;
        return range;
    }

    public static IBinding resolveBinding(ASTNode node) {
        switch (node.getNodeType()) {
            case ASTNode.TYPE_DECLARATION:
                return ((TypeDeclaration) node).resolveBinding();
            case ASTNode.METHOD_DECLARATION:
                return ((MethodDeclaration) node).resolveBinding();
            case ASTNode.FIELD_DECLARATION:
                return FieldDeclaration((FieldDeclaration) node).fragments()
                        .get(0).resolveBinding();
            case ASTNode.SINGLE_VARIABLE_DECLARATION:
                return ((SingleVariableDeclaration) node).resolveBinding();
        }

        return null;
    }

    public static String obtainIdentifier(BodyDeclaration node) {
        StringBuilder sb = new StringBuilder();

        switch (node.getNodeType()) {
            case ASTNode.ANNOTATION_TYPE_MEMBER_DECLARATION:
                sb.append(obtainIdentifier(((AnnotationTypeMemberDeclaration) node)
                        .getName()));

                break;
            case ASTNode.ANNOTATION_TYPE_DECLARATION:
            case ASTNode.ENUM_DECLARATION:
            case ASTNode.TYPE_DECLARATION:
                sb.append(((AbstractTypeDeclaration) node).getName());

                break;
            case ASTNode.ENUM_CONSTANT_DECLARATION:
                sb.append(((EnumConstantDeclaration) node).getName());

                break;
            case ASTNode.FIELD_DECLARATION:
                sb.append(FieldDeclaration((FieldDeclaration) node)
                        .getDeclaredName());

                break;
            case ASTNode.INITIALIZER:
                sb.append(node
                        .getProperty(CompilationUnitSorter.RELATIVE_ORDER));
                // int
                break;
            case ASTNode.METHOD_DECLARATION:
                _MethodDeclaration md = MethodDeclaration((MethodDeclaration) node);

                sb.append(md.getDeclaredName());

                for (SingleVariableDeclaration svd : md.parameters()) {
                    sb.append(obtainIdentifier(svd.getType()));
                    sb.append(",");
                    sb.append(obtainIdentifier(svd.getName()));
                }

                break;
        }

        return sb.toString();
    }

    public static String obtainIdentifier(Type type) {
        StringBuilder sb = new StringBuilder();

        switch (type.getNodeType()) {
            case ASTNode.SIMPLE_TYPE:
                sb.append(obtainIdentifier(((SimpleType) type).getName()));

                break;
            case ASTNode.PRIMITIVE_TYPE:
                sb.append(((PrimitiveType) type).getPrimitiveTypeCode());

                break;
            case ASTNode.ARRAY_TYPE:
                ArrayType aType = (ArrayType) type;

                sb.append(obtainIdentifier(aType.getElementType()));

                for (int i = 0; i < aType.getDimensions(); i++) {
                    sb.append("[]");
                }

                break;
        }

        return sb.toString();
    }

    public static String obtainIdentifier(Name name) {
        if (name.isSimpleName()) {
            return ((SimpleName) name).getIdentifier();
        }

        QualifiedName qName = (QualifiedName) name;
        return obtainIdentifier(qName.getQualifier()) + "."
                + obtainIdentifier(qName.getName());
    }

    public static boolean isRenamableDeclaration(final ASTNode node) {
        if (node == null) {
            return false;
        }

        Class<?>[] classes = { MethodDeclaration.class, FieldDeclaration.class,
                TypeDeclaration.class };

        return Stream.of(classes).anyMatch(s -> s.isInstance(node));
    }

    public static <T extends BodyDeclaration> _BodyDeclaration BodyDeclaration(
            T node) {
        switch (node.getNodeType()) {
            case ASTNode.METHOD_DECLARATION:
                return MethodDeclaration((MethodDeclaration) node);
            case ASTNode.FIELD_DECLARATION:
                return FieldDeclaration((FieldDeclaration) node);
            case ASTNode.TYPE_DECLARATION:
                return TypeDeclaration((TypeDeclaration) node);
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static _MethodDeclaration MethodDeclaration(MethodDeclaration node) {
        return new _MethodDeclaration(node);
    }

    public static _FieldDeclaration FieldDeclaration(FieldDeclaration node) {
        return new _FieldDeclaration(node);
    }

    public static _TypeDeclaration TypeDeclaration(TypeDeclaration node) {
        return new _TypeDeclaration(node);
    }

    /*
     * Main content.
     */

    private static class TEMP<T> {
        protected T node;

        protected TEMP(T node) {
            this.node = node;
        }
    }

    public static abstract class _BodyDeclaration<R extends BodyDeclaration>
            extends TEMP<R> {
        protected _BodyDeclaration(R node) {
            super(node);
        }

        public List<IExtendedModifier> modifiers() {
            return node.modifiers();
        }

        public abstract String getDeclaredName();

        public abstract String getParentName();
    }

    public static class _MethodDeclaration extends
            _BodyDeclaration<MethodDeclaration> {
        protected _MethodDeclaration(MethodDeclaration node) {
            super(node);
        }

        public List<TypeParameter> typeParameters() {
            return node.typeParameters();
        }

        public List<SingleVariableDeclaration> parameters() {
            return node.parameters();
        }

        @SuppressWarnings("deprecation")
        public List<Name> thrownExceptions() {
            return node.thrownExceptions();
        }

        @Override
        public String getDeclaredName() {
            return node.getName().getIdentifier();
        }

        @Override
        public String getParentName() {
            TypeDeclaration parent = (TypeDeclaration) node.getParent();
            return new _TypeDeclaration(parent).getDeclaredName();
        }
    }

    public static class _FieldDeclaration extends
            _BodyDeclaration<FieldDeclaration> {
        protected _FieldDeclaration(FieldDeclaration node) {
            super(node);
        }

        public List<VariableDeclarationFragment> fragments() {
            return node.fragments();
        }

        @Override
        public String getDeclaredName() {
            return fragments().get(0).getName().getIdentifier();
        }

        @Override
        public String getParentName() {
            TypeDeclaration parent = (TypeDeclaration) node.getParent();
            return new _TypeDeclaration(parent).getDeclaredName();
        }
    }

    public static abstract class _AbstractTypeDeclaration<R extends AbstractTypeDeclaration>
            extends _BodyDeclaration<R> {
        protected _AbstractTypeDeclaration(R node) {
            super(node);
        }

        public List<BodyDeclaration> bodyDeclarations() {
            return node.bodyDeclarations();
        }
    }

    public static class _TypeDeclaration extends
            _AbstractTypeDeclaration<TypeDeclaration> {
        protected _TypeDeclaration(TypeDeclaration node) {
            super(node);
        }

        public List<TypeParameter> typeParameters() {
            return node.typeParameters();
        }

        @SuppressWarnings("deprecation")
        public List<Name> superInterfaces() {
            return node.superInterfaces();
        }

        public List<Type> superInterfaceTypes() {
            return node.superInterfaceTypes();
        }

        @Override
        public String getDeclaredName() {
            return node.getName().getIdentifier();
        }

        @SuppressWarnings("deprecation")
        @Override
        public String getParentName() {
            return node.getSuperclass().getFullyQualifiedName();
        }
    }

    public static class _Block extends TEMP<Block> {
        protected _Block(Block node) {
            super(node);
        }

        public List<Statement> statements() {
            return node.statements();
        }
    }

    public static class _TypeParameter extends TEMP<TypeParameter> {
        protected _TypeParameter(TypeParameter node) {
            super(node);
        }

        public List<Type> typeBounds() {
            return node.typeBounds();
        }
    }

    public static class _VariableDeclaration<R extends VariableDeclaration>
            extends TEMP<R> {

        protected _VariableDeclaration(R node) {
            super(node);
        }

    }

    public static class _SingleVariableDeclaration extends
            _VariableDeclaration<SingleVariableDeclaration> {
        protected _SingleVariableDeclaration(SingleVariableDeclaration node) {
            super(node);
        }

        public List<IExtendedModifier> modifiers() {
            return node.modifiers();
        }
    }
}
