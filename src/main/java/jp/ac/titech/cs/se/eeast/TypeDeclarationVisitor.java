package jp.ac.titech.cs.se.eeast;

import jp.ac.titech.cs.se.eeast.helper.ASTHelper;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class TypeDeclarationVisitor extends
        AbstractNameBasedVisitor<TypeDeclaration> {

    public TypeDeclarationVisitor(String targetName) {
        setTargetName(targetName);
    }

    public TypeDeclarationVisitor() {
    }

    @Override
    protected String getName(TypeDeclaration node) {
        return ASTHelper.TypeDeclaration(node).getDeclaredName();
    }

    @Override
    public Class<TypeDeclaration> getAcceptableClass() {
        return TypeDeclaration.class;
    }
}
