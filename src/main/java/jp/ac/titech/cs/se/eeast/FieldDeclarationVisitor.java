package jp.ac.titech.cs.se.eeast;

import jp.ac.titech.cs.se.eeast.helper.ASTHelper;
import org.eclipse.jdt.core.dom.FieldDeclaration;

public class FieldDeclarationVisitor extends AbstractNameBasedVisitor<FieldDeclaration> {
    private static final String TAG = FieldDeclarationVisitor.class
            .getSimpleName();

    public FieldDeclarationVisitor() {
    }

    public FieldDeclarationVisitor(String name) {
        setTargetName(name);
    }

    @Override
    public Class<FieldDeclaration> getAcceptableClass() {
        return FieldDeclaration.class;
    }

    @Override
    protected String getName(FieldDeclaration node) {
        return ASTHelper.FieldDeclaration(node).getDeclaredName();
    }
}
