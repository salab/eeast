package jp.ac.titech.cs.se.eeast;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;

public interface ITypeSpecificASTVisitor<T extends ASTNode> {
    /**
     * Visits the given AST node. {@link ASTVisitorDispatcher#preVisit2(ASTNode)}
     *
     * @param node : node is instance of T {@link ITypeSpecificASTVisitor#isAcceptable(ASTNode)}
     * @return false if this should not be called again, and true otherwise.
     * @throws UnhandleASTNodeException when
     */
    boolean onVisitNode(ASTNode node) throws UnhandleASTNodeException;

    /**
     *
     * @return true if {@link ITypeSpecificASTVisitor#onVisitNode(ASTNode)} should be called, and false otherwise.
     */
    boolean isEnable();

    /**
     *
     * @param isEnable
     */
    void setEnable(boolean isEnable);

    /**
     * Check type for making {@link ITypeSpecificASTVisitor#onVisitNode(ASTNode)} be type-specific
     * @param node :
     * @return true if node is instance of T, and false otherwise.
     */
    boolean isAcceptable(ASTNode node);

    /**
     *
     * @return Class object of generic T will be accepted by this
     */
    Class<T> getAcceptableClass();

    boolean isVisited();
    void setVisited(boolean isVisited);

    List<T> getResult();
    void addResult(T result);
    void clearResult();

    void clear();

    class UnhandleASTNodeException extends IllegalAccessException {

        /**
         * Serialize version UID
         */
        private static final long serialVersionUID = 3065902336240405931L;
    }
}
