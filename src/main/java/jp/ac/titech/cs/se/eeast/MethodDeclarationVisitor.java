package jp.ac.titech.cs.se.eeast;

import jp.ac.titech.cs.se.eeast.helper.ASTHelper;
import org.eclipse.jdt.core.dom.MethodDeclaration;

public class MethodDeclarationVisitor extends
        AbstractNameBasedVisitor<MethodDeclaration> {

    public MethodDeclarationVisitor() {
    }

    public MethodDeclarationVisitor(String targetName) {
        setTargetName(targetName);
    }

    @Override
    public Class<MethodDeclaration> getAcceptableClass() {
        return MethodDeclaration.class;
    }

    @Override
    protected String getName(MethodDeclaration node) {
        return ASTHelper.MethodDeclaration(node).getDeclaredName();
    }
}