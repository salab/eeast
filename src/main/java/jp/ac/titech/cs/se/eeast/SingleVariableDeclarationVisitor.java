package jp.ac.titech.cs.se.eeast;

import org.eclipse.jdt.core.dom.SingleVariableDeclaration;

public class SingleVariableDeclarationVisitor extends AbstractNameBasedVisitor<SingleVariableDeclaration> {
    private static final String TAG = SingleVariableDeclarationVisitor.class
            .getSimpleName();

    @Override
    public Class<SingleVariableDeclaration> getAcceptableClass() {
        return SingleVariableDeclaration.class;
    }

    @Override
    protected String getName(SingleVariableDeclaration node) {
        return node.getName().getIdentifier();
    }
}
