package jp.ac.titech.cs.se.eeast.helper;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;

import java.io.*;
import java.util.Optional;

/**
 * Created by jmatsu on 2015/12/07.
 */
public class Utilities {
    private Utilities() {}

    /**
     * Returns whether are (l1, r1) and (l2, r2) overlapped on linear.
     *
     * @param l1 is left end
     * @param r1
     *            is equal and greater than l1
     * @param l2 is left end of another
     * @param r2 is not used.
     * @return
     */
    public static boolean isOverlapped(int l1, int r1, int l2, int r2) {
        int middle = getMiddleValue(l1, r1, l2);
        return (middle == l1 || middle == l2);
    }

    /**
     * Returns middle value (recursive implementation)
     *
     * @param x 1st
     * @param y 2nd
     * @param z 3rd
     * @return middle value
     */
    public static int getMiddleValue(int x, int y, int z) {
        return (x <= y) ? ((y <= z) ? y : getMiddleValue(x, z, y))
                : getMiddleValue(y, x, z);
    }

    public static Optional<String> readContent(String path) {
        try {
            return readContent(new FileInputStream(new File(path)));
        } catch (FileNotFoundException ignore) {
            return Optional.empty();
        }
    }

    public static Optional<String> readContent(File file) {
        try {
            return readContent(new FileInputStream(file));
        } catch (FileNotFoundException ignore) {
            return Optional.empty();
        }
    }

    public static Optional<String> readContent(IFile file) {
        try {
            return readContent(file.getContents());
        } catch (CoreException ignore) {
            return Optional.empty();
        }
    }

    private static Optional<String> readContent(InputStream is) {
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(is))) {
            StringBuilder sb = new StringBuilder();

            for (String line; ((line = br.readLine()) != null); sb.append(line).append(System.lineSeparator()))
                ; // FIXME Read the text of the file at once

            return Optional.of(sb.toString());
        } catch (IOException ignore) {
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ignore) {
                }
            }
        }

        return Optional.empty();
    }
}
