package jp.ac.titech.cs.se.eeast;

import jp.ac.titech.cs.se.eeast.helper.Utilities;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jdt.core.dom.ASTNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractNameBasedVisitor<T extends ASTNode> extends
        AbstractASTNodeVisitor<T> {
    private static final String TAG = AbstractNameBasedVisitor.class
            .getSimpleName();

    private static final Logger log = LoggerFactory.getLogger(TAG);

    protected String mTargetName = "";
    protected int mOffset = -1;
    protected int mLength = -1;

    public void setTargetName(String name) {
        mTargetName = name;
    }

    public void setRange(int offset, int length) {
        this.mOffset = offset;
        this.mLength = length;
    }

    protected abstract String getName(T node);

    @SuppressWarnings("unchecked")
    @Override
    public boolean onVisitNode(ASTNode node)
            throws ITypeSpecificASTVisitor.UnhandleASTNodeException {
        if (isAcceptable(node)) {
            String name = getName((T) node);

            log.debug("Node name is {}, target is {}", name, mTargetName);
            log.debug("Node range is {} to {}", node.getStartPosition(),
                    node.getStartPosition() + node.getLength());
            log.debug("Target range is {} to {}", mOffset, mOffset + mLength);

            if (name.equals(mTargetName)) {
                if (mOffset >= 0 && mLength > 0) {
                    if (Utilities.isOverlapped(mOffset, mOffset + mLength,
                            node.getStartPosition(), -1)) {
                        addResult((T) node);
                        setVisited(true);
                    }
                } else {
                    addResult((T) node);
                }
            } else if (StringUtils.isEmpty(mTargetName)) {
                addResult((T) node);
            }
        } else {
            throw new UnhandleASTNodeException();
        }

        return !isVisited();
    }

    @Override
    public void clear() {
        super.clear();
        mOffset = mLength = -1;
        mTargetName = "";
    }
}
