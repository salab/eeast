# eEAST

[![Release](https://img.shields.io/github/release/jitpack/gradle-simple.svg?label=gradle)](https://jitpack.io/#org.bitbucket.salab/eeast)
[![DUB](https://img.shields.io/dub/l/vibe-d.svg)](https://github.com/salab/eeast/blob/master/LICENSE)

Make it easy to use the abstract syntax tree provided by Eclipse JDT.

Provides type-specific API etc.

## Usage

# Add jitpack.io as Maven central repository.
```
repositories {
    // other repositories.  e.g. jcenter()
    maven { url "https://jitpack.io" }
}
```

# Add the dependency.

```
dependencies {
    compile "org.bitbucket.salab:eeast:${release_version}"
    // Each name of published tags represents ${release_version}.
    // You can choose one of them and use it.
}
```